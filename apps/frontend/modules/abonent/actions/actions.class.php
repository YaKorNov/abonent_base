<?php

/**
 * abonent actions.
 *
 * @package    sf_sandbox
 * @subpackage abonent
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class abonentActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
   
	$this->ab = Doctrine::getTable('A_B_Abonent')->getAbonentsWithAdress();
	$this->streets = Doctrine::getTable('A_B_Street')
       ->createQuery('S')
       ->execute();
	
  }
  
  public function executeGetBuildings(sfWebRequest $request)
  {
    
	if ($request->isXmlHttpRequest())
	  {
		$this->buildings = Doctrine::getTable('A_B_Building')->getRelatedBuildingsToStreet($request->getParameter('street')); 
		
		if (!$this->buildings)
		 {
		   return $this->renderText(json_encode('No results.'));
		 }
	  }
	else
		return $this->renderText('Is\'nt XmlHttpRequest');
	  
  }
  
  public function executeSearch(sfWebRequest $request)
  {
	
	if ($request->isXmlHttpRequest())
	  { 
		$this->ab = Doctrine::getTable('A_B_Abonent')->searchByDifferentFields($request->getParameter('abon'));  
		if (!$this->ab)
		   return $this->renderText('Поиск не дал результатов');
		else
		   return $this->renderPartial('abonent/list', array('abs' => $this->ab));
	  }
	else
		return $this->renderText('Is\'nt XmlHttpRequest');
	  
  }
  
  public function executeNew(sfWebRequest $request)
  {
	$this->abonent = new A_B_Abonent();
	$this->streets = Doctrine::getTable('A_B_Street')
       ->createQuery('S')
       ->execute();
  }

  public function executeCreate(sfWebRequest $request)
  {
  
	$abon_array = $request->getParameter('abon');

	$abonent = new A_B_Abonent();
	$abonent->fromArray($abon_array);

	try {
		$abonent->save();
		$execute_message = "Данные успешно сохранены";
	} catch (Exception $e) {
		$execute_message = "Ошибка при сохранении";
	}
	
	return $this->renderText($execute_message);
	
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->abonent = $this->getRoute()->getObject();
	$this->streets = Doctrine::getTable('A_B_Street')
       ->createQuery('S')
       ->execute();

	$this->buildings = Doctrine::getTable('A_B_Building')->getRelatedBuildingsToStreet($this->abonent->Street['id']);
	
  }

  public function executeUpdate(sfWebRequest $request)
  {
    $abon_array = $request->getParameter('abon');
	
	$abonent = $this->getRoute()->getObject();
    
	$abonent->synchronizeWithArray($abon_array);
	
	try {
		$abonent->save();
		$execute_message = "Данные успешно сохранены";
	} catch (Exception $e) {
		$execute_message = "Ошибка при сохранении";
	}
	
	return $this->renderText($execute_message);
  }

  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();

    $abonent = $this->getRoute()->getObject();
    $abonent->delete();

    $this->redirect('abonent/index');
  }

  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $abonent = $form->save();

      $this->redirect('abonent_edit', $abonent);
	  
    }
  }
}
