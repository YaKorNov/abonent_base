<?php slot('dojo_style', 'claro') ?>
<?php use_stylesheet('claro.css') ?>
<?php use_javascript('dojo_load.js') ?>
<?php use_javascript('dojo/dojo.js') ?>
<?php use_javascript('dojo_modules.js') ?>

<script type="text/javascript">

require(["dijit/Dialog", "dojo/domReady!"], function(Dialog){
    myDialog = new Dialog();
});
	
dojo.addOnLoad(function(){

	//var my_button = dojo.byId("Save_abon");  
    var abon_form = dojo.byId("abon_form");
	
        dojo.connect(abon_form, "onsubmit", function(event) {   
			
			dojo.stopEvent(event);
			if(!dijit.byId("abon_form").validate()){
				myDialog.set({
					title: "Ошибка",
					content: "Проверьте заполнение формы",
					style: "width: 200px"
				});
				myDialog.show();
				return false;
			}
			
			
            dojo.xhr<?php echo ($abonent->isNew() ? 'Post' : 'Put') ?>({  
                url: "<?php echo url_for('abonent/'.($abonent->isNew() ? 'create' : 'update').(!$abonent->isNew() ? '?id='.$abonent->getId() : '')) ?>",  
                form: dojo.byId("abon_form"),  
                handleAs: "text",  
                load: function(response, ioArgs) {  
              
					myDialog.set({
						title: "",
						content: response,
						style: "width: 200px"
					});
					myDialog.show();
                    return response;  
                },  
                error: function(response, ioArgs) {  
                    console.error("HTTP status code: ", ioArgs.xhr.status);  
                    return response;  
                }  
            });  
        });  
		
	
	dijit.byId("abon_name").set({required:true,regExp:'[А-Яа-я]+',missingMessage: 'Поле не заполнено!',invalidMessage:'Введите корректными данные'});
	dijit.byId("abon_surname").set({required:true,regExp:'[А-Яа-я]+',missingMessage: 'Поле не заполнено!',invalidMessage:'Введите корректными данные'});	
	dijit.byId("abon_patronymic").set({required:true,regExp:'[А-Яа-я]+',missingMessage: 'Поле не заполнено!',invalidMessage:'Введите корректными данные'});
	dijit.byId("abon_passport_ser").set({required:true,regExp:'\\d{2}\\s\\d{2}',missingMessage: 'Поле не заполнено!',invalidMessage:'пример серии паспорта - 25 13'});
	dijit.byId("abon_passport_number").set({required:true,regExp:'\\d{6}',missingMessage: 'Поле не заполнено!',invalidMessage:'пример номера паспорта - 345765'});
	dijit.byId("abon_passport_ser").set({required:true,regExp:'\\d{2}\\s\\d{2}',missingMessage: 'Поле не заполнено!',invalidMessage:'пример серии паспорта - 25 13'});
	dijit.byId("abon_birthday").set({required:true,missingMessage: 'Поле не заполнено!',invalidMessage:'Некорректный ввод'});
	dijit.byId("abon_flat").set({required:true,missingMessage: 'Поле не заполнено!',invalidMessage:'Введите корректными данные'});
	
});


function streetChange()
{ 
	var building_select = dijit.byId('abon_building_id'); 
	var street_select = dijit.byId('abon_street_id');
	var dataStore = new dojo.store.Memory({idProperty: "building_id"});
	
	if (building_select.get('disabled'))
		{
			building_select.set('disabled',false);
		}	

	building_select.set('value', '');
		
		dojo.xhrGet({  
                url: '<?php echo url_for('abonent/getBuildings?sf_format=json'); ?>',  
                content: {street: street_select.get('value')},  
                handleAs: "json",  
                load: function(response, ioArgs) {  
                     
					 // var select_html = "";
						 // for (var number in response)
						 // {
							// select_html += '<option value="' + response[number]['building_id'] + '">' + response[number]['building_title'] + '</option>';
						 // }
						 
					if (response != 'No results.')
						{
							dataStore.data = response;
							building_select.set({'store': dataStore, 'searchAttr' : 'building_title'});
						}
					else
						{
							dataStore.data = [];
							building_select.set('store', dataStore);
							console.error("Error while loading data - ", response);
						}
					
                    return response;  
                },  
                error: function(response, ioArgs) {  
                    console.error("HTTP status code: ", ioArgs.xhr.status);  
                    return response;  
                }  
            });  
}

function buildingChange(){  }
</script>


<div data-dojo-type="dijit/form/Form" data-dojo-id="abon_form" action="" id="abon_form" method="post">
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
		  <button data-dojo-type="dijit/form/Button" type="submit">Сохранить</button>
		  <?php if (!$abonent->isNew()): ?>
            &nbsp;<?php echo link_to('Удалить абонента из базы', 'abonent/delete?id='.$abonent->getId(), array('method' => 'delete', 'confirm' => 'Вы уверены?')) ?>
          <?php endif; ?>
        </td>
      </tr>
    </tfoot>
    <tbody>
      <!--?php echo $form ?-->
		<tr>
			<th><label for="abon_name">ФИО абонента</label></th>
			<td><input type="text" name="abon[surname]" data-dojo-type="dijit/form/ValidationTextBox" value="<?php if (!$abonent->isNew()) echo $abonent->getSurname();  ?>" id="abon_surname" data-dojo-props="placeHolder: 'Фамилия'"/></td>
			<td><input type="text" name="abon[name]"  data-dojo-type="dijit/form/ValidationTextBox"  value="<?php if (!$abonent->isNew()) echo $abonent->getName();  ?>" id="abon_name" data-dojo-props="placeHolder: 'Имя'"/></td>
			<td><input type="text" name="abon[patronymic]" data-dojo-type="dijit/form/ValidationTextBox" value="<?php if (!$abonent->isNew()) echo $abonent->getPatronymic();  ?>" id="abon_patronymic" data-dojo-props="placeHolder: 'Отчество'"/></td>
		</tr>
		<tr>
			<th><label for="abon_passport_ser">Данные паспорта</label></th>
			<td><input type="text" name="abon[passport_ser]"  data-dojo-type="dijit/form/ValidationTextBox" value="<?php if (!$abonent->isNew()) echo $abonent->getPassport_Ser();  ?>" id="abon_passport_ser" data-dojo-props="placeHolder: 'Серия'"/></td>
			<td><input type="text" name="abon[passport_number]"  data-dojo-type="dijit/form/ValidationTextBox" value="<?php if (!$abonent->isNew()) echo $abonent->getPassport_Number();  ?>" id="abon_passport_number" data-dojo-props="placeHolder: 'Номер'"/></td>
		</tr>
		<tr>
			<th><label for="abon_birthday">Дата рождения</label></th>
			<td><input type="text" name="abon[birthday]" value="<?php if (!$abonent->isNew()) echo $abonent->getBirthday();  ?>" id="abon_birthday" data-dojo-type="dijit/form/DateTextBox"  /></td>
		</tr>
		<tr>
			<th><label for="abon_street_id">Домашний адрес</label></th>
			<td>
				<select name="abon[street_id]" id="abon_street_id" data-dojo-type="dijit/form/FilteringSelect" 
				data-dojo-props="
				value: '<?php if (!$abonent->isNew()) echo $abonent->Street->getId();  ?>', 
				placeHolder: 'Улица',
				missingMessage: 'Поле не заполнено!',
				invalidMessage:'Некорректный ввод',
				onChange: streetChange">
					<?php foreach ($streets as $i => $street): ?>
						<option value="<?php echo $street->getId(); ?>"><?php echo $street; ?></option>
					<?php endforeach; ?>
				</select>
			</td>
			<td>
				<select name="abon[building_id]" id="abon_building_id" data-dojo-type="dijit/form/FilteringSelect" 
				data-dojo-props="
				value: '<?php if (!$abonent->isNew()) echo $abonent->Building->getId();  ?>',
				missingMessage: 'Поле не заполнено!',
				invalidMessage:'Некорректный ввод',
				placeHolder: 'Номер дома', 
				<?php if ($abonent->isNew()) echo 'disabled: true,'  ?>
				onChange: buildingChange
				">
					<?php if (!$abonent->isNew()): ?>
						<?php foreach ($buildings as $i => $building): ?>
							<option value="<?php echo $building->getId(); ?>"><?php echo $building; ?></option>
						<?php endforeach; ?>
					<?php endif; ?>
				</select>
			</td>
			<td><input type="text" name="abon[flat]" data-dojo-type="dijit/form/ValidationTextBox" value="<?php if (!$abonent->isNew()) echo $abonent->getFlat();  ?>" id="abon_flat" data-dojo-props="placeHolder: 'Квартира'"/> </td>
		</tr>
		<tr>
			<th><label for="abon_notice">Примечание</label></th>
			<td><textarea rows="2" cols="20" name="abon[notice]" id="abon_notice"><?php if (!$abonent->isNew()) echo $abonent->getNotice();  ?></textarea><br /></td>
		</tr>  
    </tbody>
  </table>
</div>
