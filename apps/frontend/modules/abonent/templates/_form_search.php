<?php slot('dojo_style', 'claro') ?>
<?php use_stylesheet('claro.css') ?>
<?php use_javascript('dojo_load.js') ?>
<?php use_javascript('dojo/dojo.js') ?>
<?php use_javascript('dojo_modules.js') ?>

<script type="text/javascript">

require(["dijit/Dialog", "dojo/domReady!"], function(Dialog){
    myDialog = new Dialog();
});

dojo.addOnLoad(function(){

	//var my_button = dojo.byId("Save_abon");  
    var search_form = dojo.byId("search_form");
	
        dojo.connect(search_form, "onsubmit", function(event) {   
			
			dojo.stopEvent(event);
			
			if(!dijit.byId("search_form").validate()){
			
				myDialog.set({
					title: "Ошибка",
					content: "Проверьте заполнение формы",
					style: "width: 200px"
				});
				myDialog.show();
				return false;
			}
			
			dojo.xhrGet ({  
                url: "<?php echo url_for('abonent/search') ?>",  
                form: dojo.byId("search_form"),  
                handleAs: "text",  
                load: function(response, ioArgs) { 
					dojo.byId('abonents').innerHTML = response; 				
                    return response;  
                },  
                error: function(response, ioArgs) {  
                    console.error("HTTP status code: ", ioArgs.xhr.status);  
                    return response;  
                }  
            });  
        });  
		
});


function streetChange()
{ 
	var building_select = dijit.byId('abon_building_id'); 
	var street_select = dijit.byId('abon_street_id');
	var dataStore = new dojo.store.Memory({idProperty: "building_id"});
	
	if (building_select.get('disabled'))
		{
			building_select.set('disabled',false);
		}	

	building_select.set('value', '');
		
		dojo.xhrGet({  
                url: '<?php echo url_for('abonent/getBuildings?sf_format=json'); ?>',  
                content: {street: street_select.get('value')},  
                handleAs: "json",  
                load: function(response, ioArgs) {  
                     
					if (response != 'No results.')
						{
							dataStore.data = response;
							building_select.set({'store': dataStore, 'searchAttr' : 'building_title'});
						}
					else
						{
							dataStore.data = [];
							building_select.set('store', dataStore);
							console.error("Error while loading data - ", response);
						}
					
                    return response;  
                },  
                error: function(response, ioArgs) {  
                    console.error("HTTP status code: ", ioArgs.xhr.status);  
                    return response;  
                }  
            });  
}

</script>

<div class="form_title"><h2>Поиск по абонентской базе</h2></div>
<br/>
<div data-dojo-type="dijit/form/Form" data-dojo-id="search_form" action="" id="search_form" method="">
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
		  <button data-dojo-type="dijit/form/Button" type="submit">Найти</button>
        </td>
      </tr>
    </tfoot>
    <tbody>
		<tr>
			<th><label>ФИО абонента</label></th>
			<td><input type="text" name="abon[surname]" data-dojo-type="dijit/form/ValidationTextBox" value="" id="abon_surname" data-dojo-props="placeHolder: 'Фамилия'"/></td>
			<td><input type="text" name="abon[name]"  data-dojo-type="dijit/form/ValidationTextBox"  value="" id="abon_name" data-dojo-props="placeHolder: 'Имя'"/></td>
			<td><input type="text" name="abon[patronymic]" data-dojo-type="dijit/form/ValidationTextBox" value="" id="abon_patronymic" data-dojo-props="placeHolder: 'Отчество'"/></td>
		</tr>
		<tr>
			<th><label>Данные паспорта</label></th>
			<td><input type="text" name="abon[passport_ser]"  data-dojo-type="dijit/form/ValidationTextBox" value="" id="abon_passport_ser" data-dojo-props="placeHolder: 'Серия'"/></td>
			<td><input type="text" name="abon[passport_number]"  data-dojo-type="dijit/form/ValidationTextBox" value="" id="abon_passport_number" data-dojo-props="placeHolder: 'Номер'"/></td>
		</tr>
		<tr>
			<th><label>Дата рождения</label></th>
			<td><input type="text" name="abon[birthday]" value="" id="abon_birthday" data-dojo-type="dijit/form/DateTextBox" data-dojo-props="invalidMessage:'Некорректный ввод'" /></td>
		</tr>
		<tr>
			<th><label>Домашний адрес</label></th>
			<td>
				<select name="abon[street_id]" id="abon_street_id" data-dojo-type="dijit/form/FilteringSelect" 
				data-dojo-props="
				value: '', 
				placeHolder: 'Улица',
				required: false,
				invalidMessage:'Некорректный ввод',
				onChange: streetChange">
					<?php foreach ($streets as $i => $street): ?>
						<option value="<?php echo $street->getId(); ?>"><?php echo $street; ?></option>
					<?php endforeach; ?>
				</select>
			</td>
			<td>
				<select name="abon[building_id]" id="abon_building_id" data-dojo-type="dijit/form/FilteringSelect" 
				data-dojo-props="
				value: '',
				required: false,
				invalidMessage:'Некорректный ввод',
				placeHolder: 'Номер дома', 
				disabled: true">
				</select>
			</td>
			<td><input type="text" name="abon[flat]" data-dojo-type="dijit/form/ValidationTextBox" value="" id="abon_flat" data-dojo-props="placeHolder: 'Квартира'"/> </td>
		</tr>
		<tr>
			<th><label for="abon_notice">Примечание</label></th>
			<td colspan="3"><textarea name="abon[notice]" id="abon_notice"></textarea><br /></td>
		</tr>  
    </tbody>
  </table>
</div>
