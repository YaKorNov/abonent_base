﻿<table class="abonents">
	<tr class="">
      <td class="surname">
        <strong>Фамилия</strong>
      </td>
      <td class="name">
        <strong>Имя</strong>
      </td>
	  <td class="patronymic">
        <strong>Отчество</strong>
      </td>
	  <td class="address">
        <strong>Домашний адрес</strong>
      </td>
	  <td class="birthday">
        <strong>Примечание</strong>
      </td>
    </tr>
  <?php foreach ($abs as $i => $ab): ?>
    <tr class="<?php echo fmod($i, 2) ? 'even' : 'odd' ?>">
      <td class="surname">
        <?php echo $ab->getSurname() ?>
      </td>
      <td class="name">
        <?php echo  link_to( $ab->getName(),'abonent_edit', $ab) ?>
      </td>
	  <td class="patronymic">
        <?php echo $ab->getPatronymic() ?>
      </td>
	  <td class="address">
        <?php echo $ab->get_adress_string() ?>
      </td>
	  <td class="birthday">
        <?php echo $ab->getNotice(); ?>
      </td>
    </tr>
  <?php endforeach; ?>
</table>