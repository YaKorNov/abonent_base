<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <link rel="shortcut icon" href="/favicon.ico" />
    <?php include_stylesheets() ?>
    <?php include_javascripts() ?>
  </head>
  <body class="<?php include_slot('dojo_style') ?>">
	<div id="container">
	
      <div id="header-wrap">
        <div class="header">
		  <div class="Title">
			  <h1>
				  <a href="<?php echo url_for('@homepage') ?>">
					Абонентская база
				  </a>  
			  </h1>
		  </div>
		  <div class="Register">
			<div class="button_round">
				<h3><a href="<?php echo url_for('abonent/new') ?>">Регистрация нового абонента</a></h3>
			</div>
		  </div>
		  <div class="clear"></div>
        </div>
      </div>
 
      <div id="content-wrap">
        <div class="content">
          <?php echo $sf_content ?>
        </div>
      </div>
 
      <div id="footer-wrap">
        <div class="footer">
          <div class="abonent_base">
            Сделано в 2013 году :)
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
