<?php

/**
 * A_B_Abonent filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseA_B_AbonentFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'name'            => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'surname'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'patronymic'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'passport_ser'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'passport_number' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'birthday'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'street_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Street'), 'add_empty' => true)),
      'building_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Building'), 'add_empty' => true)),
      'flat'            => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'notice'          => new sfWidgetFormFilterInput(),
      'created_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'name'            => new sfValidatorPass(array('required' => false)),
      'surname'         => new sfValidatorPass(array('required' => false)),
      'patronymic'      => new sfValidatorPass(array('required' => false)),
      'passport_ser'    => new sfValidatorPass(array('required' => false)),
      'passport_number' => new sfValidatorPass(array('required' => false)),
      'birthday'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'street_id'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Street'), 'column' => 'id')),
      'building_id'     => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Building'), 'column' => 'id')),
      'flat'            => new sfValidatorPass(array('required' => false)),
      'notice'          => new sfValidatorPass(array('required' => false)),
      'created_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('a_b_abonent_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'A_B_Abonent';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'name'            => 'Text',
      'surname'         => 'Text',
      'patronymic'      => 'Text',
      'passport_ser'    => 'Text',
      'passport_number' => 'Text',
      'birthday'        => 'Date',
      'street_id'       => 'ForeignKey',
      'building_id'     => 'ForeignKey',
      'flat'            => 'Text',
      'notice'          => 'Text',
      'created_at'      => 'Date',
      'updated_at'      => 'Date',
    );
  }
}
