<?php

/**
 * A_B_Abonent form base class.
 *
 * @method A_B_Abonent getObject() Returns the current form's model object
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseA_B_AbonentForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'name'            => new sfWidgetFormInputText(),
      'surname'         => new sfWidgetFormInputText(),
      'patronymic'      => new sfWidgetFormInputText(),
      'passport_ser'    => new sfWidgetFormInputText(),
      'passport_number' => new sfWidgetFormInputText(),
      'birthday'        => new sfWidgetFormDate(),
      'street_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Street'), 'add_empty' => false)),
      'building_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Building'), 'add_empty' => false)),
      'flat'            => new sfWidgetFormInputText(),
      'notice'          => new sfWidgetFormTextarea(),
      'created_at'      => new sfWidgetFormDateTime(),
      'updated_at'      => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'name'            => new sfValidatorString(array('max_length' => 255)),
      'surname'         => new sfValidatorString(array('max_length' => 255)),
      'patronymic'      => new sfValidatorString(array('max_length' => 255)),
      'passport_ser'    => new sfValidatorString(array('max_length' => 255)),
      'passport_number' => new sfValidatorString(array('max_length' => 255)),
      'birthday'        => new sfValidatorDate(),
      'street_id'       => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Street'))),
      'building_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Building'))),
      'flat'            => new sfValidatorString(array('max_length' => 255)),
      'notice'          => new sfValidatorString(array('max_length' => 1000, 'required' => false)),
      'created_at'      => new sfValidatorDateTime(),
      'updated_at'      => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('a_b_abonent[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'A_B_Abonent';
  }

}
