<?php

/**
 * A_B_Abonent form.
 *
 * @package    sf_sandbox
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class A_B_AbonentForm extends BaseA_B_AbonentForm
{
  public function configure()
  {
  
	unset(
      $this['created_at'], $this['updated_at']
    );

	$this->widgetSchema['birthday'] = new sfWidgetFormInputText();
	
	$this->widgetSchema->setLabels(array(
	  'name'    => 'Имя',
	  'surname'      => 'Фамилия',
	  'patronymic'   => 'Отчество',
	  'passport_ser'    => 'Серия паспорта',
	  'passport_number'      => 'Номер паспорта',
	  'birthday'   => 'Дата рождения',
	  'street_id'    => 'Улица',
	  'building_id'      => 'Дом',
	  'flat'   => 'Квартира',
	  'notice'    => 'Примечание',
	));
	
	$this->widgetSchema->setHelp('notice', 'Дополнительная информация');
	
	// $streets = $this->widgetSchema['street_id']->getChoices();
	
	
	// $BuildingsQuery = Doctrine::getTable('A_B_Building')->getRelatedBuildingsToStreetQuery($streets[0]);

     // $this->widgetSchema['building_id']->setOption('query', $BuildingsQuery);
    // $this->validatorSchema['building_id']->setOption('query', $BuildingsQuery);
	
	 $this->widgetSchema['building_id']->setOption('order_by', array('building_title','asc'));
	
	//$this->widgetSchema['building_id']->setOption('table_method', 'getBuildingsRelatedToFirstStreet');
   // $this->validatorSchema['building_id']->setOption('table_method', 'getBuildingsRelatedToFirstStreet');
	
	
	$this->widgetSchema->setNameFormat('abon[%s]');
  
  }
}
